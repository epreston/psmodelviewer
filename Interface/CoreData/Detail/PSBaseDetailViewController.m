//
//  PSBaseDetailViewController.m
//  - Not project specific
//
//  Created by Ed Preston on 7/5/10.
//  Copyright 2010 Preston Software. All rights reserved.
//

#import "PSBaseDetailViewController.h"


@interface PSBaseDetailViewController ()
{
    
@private
	UIToolbar				*__weak toolbar_;
    NSManagedObjectModel    *managedObjectModel_;
}

@end


@implementation PSBaseDetailViewController

@synthesize	toolbar                 = toolbar_;
@synthesize managedObjectModel      = managedObjectModel_;


// IMPLEMENT IN SUBCLASS: Update the user interface for the detail item.

- (void) configureView 
{		
//	// The nib will overwrite our configuration if we are not loaded
//	if ( [self isViewLoaded] ) {
//		
//		
//	}
}


#pragma mark - View Lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void) viewDidLoad 
{    
	[super viewDidLoad];
	
	[self configureView];
}

#pragma mark - Resource Management

- (void) didReceiveMemoryWarning 
{    
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

- (void) viewDidUnload 
{		
	self.toolbar = nil;
    
    [super viewDidUnload];
}



#pragma mark - PSSwappableDetailView

- (void) showRootPopoverButtonItem:(UIBarButtonItem *)barButtonItem 
{    
    // Add the popover button to the toolbar.
    NSMutableArray *itemsArray = [toolbar_.items mutableCopy];
	if ( [itemsArray indexOfObject:barButtonItem] == NSNotFound ) {
		[itemsArray insertObject:barButtonItem atIndex:0];
	} else {
        // This may seem silly but is nessary, button will disappear when displaying the
        // detailViewController for a second time in portrait.
        itemsArray[[itemsArray indexOfObject:barButtonItem]] = barButtonItem;
    }
    [toolbar_ setItems:itemsArray animated:NO];
}

- (void) invalidateRootPopoverButtonItem:(UIBarButtonItem *)barButtonItem 
{    
    // Remove the popover button from the toolbar.
    NSMutableArray *itemsArray = [toolbar_.items mutableCopy];
    [itemsArray removeObject:barButtonItem];
    [toolbar_ setItems:itemsArray animated:NO];
}


#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField 
{	
	// Dismiss the keyboard when they press return
	[textField resignFirstResponder];
	return NO;
}


#pragma mark - Rotation Support

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{   
    // Ensure that the view controller supports rotation and that the split view can therefore
    // show in both portrait and landscape.
    
	return YES;
}


@end
